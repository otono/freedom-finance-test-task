<?php

namespace Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CourseControllerTest extends WebTestCase
{
    public function test_request_responded_successful_result(): void
    {
        $client = static::createClient();
        $client->request(Request::METHOD_GET, '/api/v1/course', [
            'date' => '2023-07-17',
            'currency' => 'USD',
            'base_currency' => 'RUB'
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertResponseStatusCodeSame(Response::HTTP_OK);

        $json = json_decode($client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('course', $json);
        $this->assertArrayHasKey('previousDayDiff', $json);
    }
}