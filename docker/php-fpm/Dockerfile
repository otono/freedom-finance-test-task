FROM php:8.2-fpm-alpine

RUN apk add --no-cache curl git build-base zlib-dev oniguruma-dev autoconf bash rabbitmq-c-dev supervisor
RUN apk add --update linux-headers

# Symfony CLI
RUN curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash \
    && apk add symfony-cli

RUN pecl install redis amqp && docker-php-ext-enable redis \
    && docker-php-ext-install bcmath \
    && docker-php-ext-enable amqp

# Xdebug
ARG INSTALL_XDEBUG=false
RUN if [ ${INSTALL_XDEBUG} = true ]; \
    then \
      pecl install xdebug && docker-php-ext-enable xdebug; \
    fi;

# Set www-data user
ARG PUID=1000
ARG PGID=1000
RUN apk --no-cache add shadow && \
    groupmod -o -g ${PGID} www-data && \
    usermod -o -u ${PUID} -g www-data www-data

COPY ./docker/php-fpm/xdebug.ini /usr/local/etc/php/conf.d/xdebug.ini

RUN chown www-data:www-data /var/www
COPY --chown=www-data:www-data ./ /var/www
WORKDIR /var/www

RUN chmod -R 777 /var/www/storage/*

# Build script
COPY ./docker/php-fpm/build-app.sh /usr/local/bin
RUN chmod +x /usr/local/bin/*

# Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

EXPOSE 80