<?php

namespace App\Exception;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

class ValidationException extends \Exception implements ValidationExceptionInterface
{
    private ConstraintViolationListInterface $violations;

    /**
     * @param ConstraintViolationListInterface $violations
     * @param string $message
     */
    public function __construct(ConstraintViolationListInterface $violations, string $message = "")
    {
        parent::__construct($message, Response::HTTP_BAD_REQUEST);
        $this->violations = $violations;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        $errors = [];
        foreach ($this->violations as $violation) {
            $prop = str_replace('[', '', str_replace(']', '', $violation->getPropertyPath()));
            $errors[$prop][] = $violation->getMessage();
        }

        return $errors;
    }
}