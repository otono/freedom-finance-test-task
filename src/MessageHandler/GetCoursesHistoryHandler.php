<?php

namespace App\MessageHandler;

use App\Message\GetCoursesHistory;
use App\Service\Course\CourseDataService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class GetCoursesHistoryHandler
{
    private LoggerInterface $coursesLogger;
    private CourseDataService $courseDataService;

    /**
     * @param LoggerInterface $coursesLogger
     * @param CourseDataService $courseDataService
     */
    public function __construct(LoggerInterface $coursesLogger, CourseDataService $courseDataService)
    {
        $this->coursesLogger = $coursesLogger;
        $this->courseDataService = $courseDataService;
    }

    /**
     * @throws \Exception
     */
    public function __invoke(GetCoursesHistory $message)
    {
        // Timeout
        sleep(1);

        // Log
        $this->coursesLogger->info('Started get courses for date: ' . $message->getDate());

        // Update courses
        try {
            $this->courseDataService->updateCoursesOnDate(new \DateTime($message->getDate()));
        } catch (\Exception $e) {
            $this->coursesLogger->error('[ERROR] Error while updating courses on date: ' . $message->getDate());
            exit;
        }

        // Log
        $this->coursesLogger->info('[OK] Courses updated for date: '. $message->getDate());
    }
}