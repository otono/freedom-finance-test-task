<?php

namespace App\EventListener;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\Validator\Exception\ValidationFailedException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $response = [
            'message' => 'ERROR: '.$exception->getMessage(),
        ];

        // Validation
        if ($exception->getPrevious() instanceof ValidationFailedException) {
            $errors = [];
            foreach ($exception->getPrevious()->getViolations() as $violation) {
                $prop = str_replace('[', '', str_replace(']', '', $violation->getPropertyPath()));
                $errors[$prop][] = $violation->getMessage();
            }
            $response['message'] = 'Validation error';
            $response['errors'] = $errors;
        }

        if ($exception instanceof ValidationException) {
            $response['errors'] = $exception->getErrors();
        }

        $event->setResponse(new JsonResponse($response));
    }
}