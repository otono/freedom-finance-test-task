<?php

namespace App\Service\Course;

use App\Exception\CurrencyNotExistsException;
use App\Integration\CBR\Service\CBRService;
use Symfony\Contracts\Cache\CacheInterface;

class CourseDataService
{
    private \Redis $redis;
    private CBRService $cbrService;
    private DateService $dateService;

    /**
     * @param CBRService $cbrService
     * @param CacheInterface $cacheCourse
     * @param DateService $dateService
     */
    public function __construct(CBRService $cbrService, CacheInterface $cacheCourse, DateService $dateService)
    {
        $this->redis = $cacheCourse::createConnection(getenv('REDIS_HOST'));
        $this->cbrService = $cbrService;
        $this->dateService = $dateService;
    }

    /**
     * @param \DateTime $date
     * @param string $currency
     * @param string $baseCurrency
     * @return array
     */
    public function getCourse(\DateTime $date, string $currency, string $baseCurrency): array
    {
        return [
            'course' => $this->redis->hGet('course:' . $date->format('Y-m-d'), $currency),
            'base_course' => $this->redis->hGet('course:' . $date->format('Y-m-d'), $baseCurrency),
            'nominal' => $this->redis->hGet('nominal', $currency),
            'base_nominal' => $this->redis->hGet('nominal', $baseCurrency),
        ];
    }

    /**
     * @param \DateTime $date
     * @param string $currency
     * @param string $baseCurrency
     * @return void
     * @throws \Exception
     */
    public function updateIfNotExists(\DateTime $date, string $currency, string $baseCurrency): void
    {
        if (
            empty($this->redis->hGet('course:'.$date->format('Y-m-d'), $currency))
            || empty($this->redis->hGet('course:'.$date->format('Y-m-d'), $baseCurrency))
        ) {
            foreach ([$date, $this->dateService->getPreviousDate($date)] as $courseDate) {
                $this->updateData($courseDate);
            }
        }
    }

    /**
     * @param \DateTime $date
     * @return void
     */
    private function updateData(\DateTime $date): void
    {
        $courseResponse = $this->cbrService->getCoursesOnDate($date);

        $nominal = [];  // Currency nominal data
        $courses = [];  // Courses data

        foreach ($courseResponse->getCourses() as $course) {
            $nominal[$course->getCharCode()] = $course->getNominal();
            $courses[$course->getCharCode()] = str_replace(',', '.', $course->getValue());
        }

        // Set courses
        $this->redis->hMSet('course:' . $date->format('Y-m-d'), $courses);
        // Set nominal
        $this->redis->hMSet('nominal', $nominal);
    }

    /**
     * @param \DateTime $date
     * @return void
     */
    public function updateCoursesOnDate(\DateTime $date): void
    {
        $courseResponse = $this->cbrService->getCoursesOnDate($date);

        $courses = [];
        foreach ($courseResponse->getCourses() as $course) {
            $courses[$course->getCharCode()] = str_replace(',', '.', $course->getValue());
        }

        // Store
        $this->redis->hMSet('course:' . $date->format('Y-m-d'), $courses);
    }

    /**
     * @param array $currencies
     * @return void
     * @throws CurrencyNotExistsException
     */
    public function checkCurrencyExists(array $currencies)
    {
        $nominals = $this->redis->hGetAll('nominal');

        foreach ($currencies as $currency) {
            if (!isset($nominals[$currency]) && $currency !== getenv('DEFAULT_BASE_CURRENCY')) {
                throw new CurrencyNotExistsException("Currency {$currency} not exists.");
            }
        }
    }
}