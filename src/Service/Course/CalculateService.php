<?php

namespace App\Service\Course;

class CalculateService
{
    /**
     * @param string $course
     * @param string $baseCourse
     * @param string $nominal
     * @param string $baseNominal
     * @return string
     */
    public function calculateCrossCourse(string $course, string $baseCourse, string $nominal, string $baseNominal): string
    {
        if (empty($baseCourse)) {
            return $course;
        }

        if (empty($course)) {
            return bcdiv((string) $nominal, $baseCourse, 4);
        }

        $invertCourse = bcdiv($nominal, $course, 8);
        $invertBaseCourse = bcdiv($baseNominal, $baseCourse, 8);

        return bcdiv($invertBaseCourse, $invertCourse, 4);
    }
}