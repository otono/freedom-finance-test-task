<?php

namespace App\Service\Course;

class DateService
{
    const EMPTY_WEEKDAYS = [1, 7];
    const DAY_SHIFT = [1 => -2, 7 => -1];

    /**
     * @param \DateTime $date
     * @return \DateTime
     */
    public function getActualDate(\DateTime $date): \DateTime
    {
        $actualDate = clone $date;
        $weekDay = (int) $actualDate->format('N');

        // If Monday or Sunday, get previous saturday date
        if (\in_array($weekDay, self::EMPTY_WEEKDAYS)) {
            $actualDate->modify('-' . self::DAY_SHIFT[$weekDay] . ' days');
        }

        return $actualDate;
    }

    /**
     * @param \DateTime $date
     * @return \DateTime
     * @throws \Exception
     */
    public function getPreviousDate(\DateTime $date): \DateTime
    {
        $previousDate = clone $date;

        return $this->getActualDate($previousDate->modify('- 1 day'));
    }
}