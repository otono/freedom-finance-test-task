<?php

namespace App\Service\Course;

use App\DTO\Response\CrossCourseResponseDTO;

class CrossCourseService
{
    private DateService $dateService;
    private CourseDataService $courseDataService;
    private CalculateService $calculateService;

    /**
     * @param DateService $dateService
     * @param CourseDataService $courseDataService
     * @param CalculateService $calculateService
     */
    public function __construct(DateService $dateService, CourseDataService $courseDataService, CalculateService $calculateService)
    {
        $this->dateService = $dateService;
        $this->courseDataService = $courseDataService;
        $this->calculateService = $calculateService;
    }

    /**
     * @param string $date
     * @param string $currency
     * @param string|null $baseCurrency
     * @return CrossCourseResponseDTO
     * @throws \Exception
     */
    public function getCourseOnDate(string $date, string $currency, ?string $baseCurrency): CrossCourseResponseDTO
    {
        $date = $this->dateService->getActualDate(new \DateTime($date));
        $baseCurrency = $baseCurrency ?? getenv('DEFAULT_BASE_CURRENCY');

        // Update data if not exists
        $this->courseDataService->updateIfNotExists($date, $currency, $baseCurrency);

        // Check currencies exists
        $this->courseDataService->checkCurrencyExists([$currency, $baseCurrency]);

        // Get courses
        $courses = [];
        foreach ([$date, $this->dateService->getPreviousDate($date)] as $courseDate) {
            $courses[] = $this->courseDataService->getCourse($courseDate, $currency, $baseCurrency);
        }

        // Calculate cross-courses
        $crossCourses = [];
        foreach ($courses as $data) {
            $crossCourses[] = $this->calculateService->calculateCrossCourse(
                $data['course'], $data['base_course'], $data['nominal'], $data['base_nominal']
            );
        }

        $crossCourseResponseDTO = new CrossCourseResponseDTO();
        $crossCourseResponseDTO->setCourse($crossCourses[0]);

        $diff = bcsub($crossCourses[0], $crossCourses[1], 4);
        $sign = (!str_contains($diff, '-')) ? ((float) $diff == 0) ? '' : '+' : '';
        $crossCourseResponseDTO->setPreviousDayDiff($sign . $diff);

        return $crossCourseResponseDTO;
    }
}