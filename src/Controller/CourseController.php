<?php

namespace App\Controller;

use App\DTO\Request\CrossCourseRequestDTO;
use App\Service\Course\CrossCourseService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;

class CourseController extends AbstractController
{
    /**
     * @param CrossCourseRequestDTO $query
     * @param CrossCourseService $crossCourseService
     * @return JsonResponse
     * @throws \Exception
     */
    #[Route('/api/v1/course', name: 'app_course', methods: ['GET'])]
    public function index(
        #[MapQueryString] CrossCourseRequestDTO $query,
        CrossCourseService $crossCourseService
    ): JsonResponse
    {
        $course = $crossCourseService->getCourseOnDate($query->getDate(), $query->getCurrency(), $query->getBaseCurrency());

        return $this->json($course);
    }
}
