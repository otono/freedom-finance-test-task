<?php

namespace App\DTO\Response;

class CrossCourseResponseDTO
{
    private string $course;
    private string $previousDayDiff;

    /**
     * @return string
     */
    public function getCourse(): string
    {
        return $this->course;
    }

    /**
     * @param string $course
     */
    public function setCourse(string $course): void
    {
        $this->course = $course;
    }

    /**
     * @return string
     */
    public function getPreviousDayDiff(): string
    {
        return $this->previousDayDiff;
    }

    /**
     * @param string $previousDayDiff
     */
    public function setPreviousDayDiff(string $previousDayDiff): void
    {
        $this->previousDayDiff = $previousDayDiff;
    }
}