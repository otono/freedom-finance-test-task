<?php

namespace App\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;

class CrossCourseRequestDTO
{
    #[Assert\NotBlank]
    #[Assert\Date]
    private string $date;

    #[Assert\NotBlank]
    #[Assert\Length(exactly: 3)]
    private string $currency;

    private string $baseCurrency;

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return string
     */
    public function getBaseCurrency(): string
    {
        return $this->baseCurrency;
    }

    /**
     * @param string $baseCurrency
     */
    public function setBaseCurrency(string $baseCurrency): void
    {
        $this->baseCurrency = $baseCurrency;
    }


}