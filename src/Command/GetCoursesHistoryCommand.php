<?php

namespace App\Command;

use App\Message\GetCoursesHistory;
use App\Service\Course\DateService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

#[AsCommand(
    name: 'app:get-courses-history',
    description: 'Dispatches queues to get CBR courses for last 180 days.',
)]
class GetCoursesHistoryCommand extends Command
{
    const DAYS = 180;

    private DateService $dateService;
    private MessageBusInterface $bus;

    /**
     * @param DateService $dateService
     * @param MessageBusInterface $bus
     */
    public function __construct(DateService $dateService, MessageBusInterface $bus)
    {
        parent::__construct();
        $this->dateService = $dateService;
        $this->bus = $bus;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->info('Dispatching get courses history queues');

        $processed = [];
        $date = new \DateTime();

        for ($i = 0; $i <= self::DAYS; $i++) {
            $date->modify('-1 day');
            $validDate = $this->dateService->getActualDate($date);

            // Dispatch
            if (!isset($processed[$validDate->format('Y-m-d')])) {
                $this->bus->dispatch(new GetCoursesHistory($validDate->format('Y-m-d')));
                $processed[$validDate->format('Y-m-d')] = true;
            }
        }

        $io->success('All messages sent.');

        return Command::SUCCESS;
    }
}
