<?php

namespace App\Integration\CBR\Client;

interface CBRClientInterface
{
    public function getCoursesOnDate(\DateTime $date): string;
}