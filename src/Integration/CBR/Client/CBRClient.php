<?php

namespace App\Integration\CBR\Client;


use GuzzleHttp\Client;

class CBRClient implements CBRClientInterface
{
    private Client $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param \DateTime $date
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCoursesOnDate(\DateTime $date): string
    {
        return $this->request([
            'date_req' => $date->format('d/m/Y')
        ]);
    }

    /**
     * @param array $data
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request(array $data): string
    {
        $response = $this->client->request('GET', getenv('CBR_COURSES_API'), [
            'headers' => [
                'Accept' => 'application/xml'
            ],
            'query' => $data
        ]);

        return $response->getBody()->getContents();
    }
}