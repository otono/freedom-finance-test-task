<?php

namespace App\Integration\CBR\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class CourseDTO
{
    #[SerializedName('@ID')]
    private string $id;
    private string $numCode;
    private string $charCode;
    private string $nominal;
    private string $name;
    private string $value;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getNumCode(): string
    {
        return $this->numCode;
    }

    /**
     * @param string $numCode
     */
    public function setNumCode(string $numCode): void
    {
        $this->numCode = $numCode;
    }

    /**
     * @return string
     */
    public function getCharCode(): string
    {
        return $this->charCode;
    }

    /**
     * @param string $charCode
     */
    public function setCharCode(string $charCode): void
    {
        $this->charCode = $charCode;
    }

    /**
     * @return string
     */
    public function getNominal(): string
    {
        return $this->nominal;
    }

    /**
     * @param string $nominal
     */
    public function setNominal(string $nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void
    {
        $this->value = $value;
    }
}