<?php

namespace App\Integration\CBR\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class CoursesResponseDTO
{
    #[SerializedName('@name')]
    private string $name;

    #[SerializedName('@Date')]
    private \DateTime $date;

    private array $courses;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate(\DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return array
     */
    public function getCourses(): array
    {
        return $this->courses;
    }

    /**
     * @param array $courses
     */
    public function setCourses(array $courses): void
    {
        $this->courses = $courses;
    }

    /**
     * @param CourseDTO $course
     * @return void
     */
    public function addCourse(CourseDTO $course): void
    {
        $this->courses[] = $course;
    }
}