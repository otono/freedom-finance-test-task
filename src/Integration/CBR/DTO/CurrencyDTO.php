<?php

namespace App\Integration\CBR\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class CurrencyDTO
{
    #[SerializedName('@ID')]
    private string $id;
    private string $name;
    private string $engName;
    private int $nominal;
    private string $parentCode;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEngName(): string
    {
        return $this->engName;
    }

    /**
     * @param string $engName
     */
    public function setEngName(string $engName): void
    {
        $this->engName = $engName;
    }

    /**
     * @return int
     */
    public function getNominal(): int
    {
        return $this->nominal;
    }

    /**
     * @param int $nominal
     */
    public function setNominal(int $nominal): void
    {
        $this->nominal = $nominal;
    }

    /**
     * @return string
     */
    public function getParentCode(): string
    {
        return $this->parentCode;
    }

    /**
     * @param string $parentCode
     */
    public function setParentCode(string $parentCode): void
    {
        $this->parentCode = $parentCode;
    }
}