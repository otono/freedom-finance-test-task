<?php

namespace App\Integration\CBR\DTO;

use Symfony\Component\Serializer\Annotation\SerializedName;

class CurrenciesResponseDTO
{
    #[SerializedName('@name')]
    private string $name;

    #[SerializedName('Item')]
    private array $currencies;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return array
     */
    public function getCurrencies(): array
    {
        return $this->currencies;
    }

    /**
     * @param array $currencies
     */
    public function setCurrencies(array $currencies): void
    {
        $this->currencies = $currencies;
    }

    /**
     * @param CurrencyDTO $item
     * @return void
     */
    public function addCurrency(CurrencyDTO $item): void
    {
        $this->currencies[] = $item;
    }
}