<?php

namespace App\Integration\CBR\Service;

use App\Integration\CBR\Client\CBRClientInterface;
use App\Integration\CBR\DTO\CoursesResponseDTO;
use Symfony\Component\Serializer\SerializerInterface;

class CBRService
{
    private CBRClientInterface $client;
    private SerializerInterface $serializer;

    /**
     * @param CBRClientInterface $client
     * @param SerializerInterface $serializer
     */
    public function __construct(CBRClientInterface $client, SerializerInterface $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    /**
     * @param \DateTime $date
     * @return CoursesResponseDTO
     */
    public function getCoursesOnDate(\DateTime $date): CoursesResponseDTO
    {
        $data = $this->client->getCoursesOnDate($date);
        return $this->serializer->deserialize($data, 'App\Integration\CBR\DTO\CoursesResponseDTO', 'xml');
    }
}