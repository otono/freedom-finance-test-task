<?php

namespace App\Integration\CBR\Serializer;

use App\Integration\CBR\DTO\CourseDTO;
use App\Integration\CBR\DTO\CoursesResponseDTO;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @method  getSupportedTypes(?string $format)
 */
class CoursesResponseDTODenormalizer implements DenormalizerInterface
{
    private ObjectNormalizer $normalizer;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(ObjectNormalizer $normalizer)
    {
        $this->normalizer = $normalizer;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param string|null $format
     * @param array $context
     * @return mixed
     * @throws \Exception
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): CoursesResponseDTO
    {
        $object = new $type();
        $object->setName($data['@name']);
        $object->setDate(new \DateTime($data['@Date']));

        foreach ($data['Valute'] as $item) {
            $course = $this->normalizer->denormalize($item, CourseDTO::class);
            $course->setId($item['@ID']);
            $object->addCourse($course);
        }

        return $object;
    }

    /**
     * @param mixed $data
     * @param string $type
     * @param string|null $format
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null): bool
    {
        return $type == CoursesResponseDTO::class;
    }
}