<?php

namespace App\Message;

class GetCoursesHistory
{
    public function __construct(
        private string $date,
    ) {
    }

    public function getDate(): string
    {
        return $this->date;
    }
}