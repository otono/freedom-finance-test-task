SHELL := /bin/bash

.PHONY: docker-run
docker-run: # build & start
	cd docker && docker-compose up --build

.PHONY: docker-build
docker-build:
	cd docker && docker-compose build

.PHONY: docker-up
docker-start:
	cd docker && docker-compose up

.PHONY: install
install:
	docker exec -u www-data -it ff-php-fpm build-app.sh

.PHONY: bash
bash:
	docker exec -u www-data -it ff-php-fpm bash

.PHONY: redis-cli
redis-cli:
	docker exec -it ff-redis redis-cli

.PHONY: rabbit-bash
rabbit-bash:
	docker exec -it ff-rabbit bash

.PHONY: tests
tests:
	export APP_ENV=test
	APP_ENV=test php bin/phpunit $@
