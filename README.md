# Freedom Finance test task

To start application run following commands:

#### 1. Build & start docker
```sh
make docker-run
```

OR

```sh
cd docker && docker-compose up --build
```

#### 2. Build application
```sh
make install
```

OR

```sh
docker exec -u www-data -it ff-php-fpm build-app.sh
```

### Get cross-course
```sh
curl --request GET \
  --url 'http://localhost:8082/api/v1/course?date=2023-07-17&currency=USD&base_currency=RUB'
````
GET parameters:
- date (yyyy-mm-dd) *
- currency (code) *
- base_currency (code)

### Console command to get last 180 days courses history
1. Open container's command line
```sh
make bash
```
OR
```sh
docker exec -u www-data -it ff-php-fpm bash
```
2. Execute get history command
```sh
php bin/console app:get-courses-history
```

### API Doc
http://localhost:8082/api/doc

### RabbitMQ admin
http://localhost:15671/

user: rabbit

password: rabbit